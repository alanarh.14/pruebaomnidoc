package prueba.omnidoc.alan.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AlbumModel {
    var href: String = ""
    var items: List<Album> = ArrayList()
    var limit: String = ""
    var next: String = ""
    var offset: String = ""
    var previous: String = ""
    var total: String = ""
}

class Album : Serializable {
    @SerializedName("album_group")
    @Expose
    val album_group: String = ""

    @SerializedName("album_type")
    @Expose
    val album_type: String = ""

    @SerializedName("artists")
    @Expose
    val artists: List<Artista> = ArrayList()

    @SerializedName("external_urls")
    @Expose
    val external_urls: List<ExternalUrls> = ArrayList()

    @SerializedName("href")
    @Expose
    val href: String = ""

    @SerializedName("id")
    @Expose
    val id: String = ""

    @SerializedName("images")
    @Expose
    val images: List<Images> = ArrayList()

    @SerializedName("name")
    @Expose
    val name: String = ""

    @SerializedName("release_date")
    @Expose
    val release_date: String = ""

    @SerializedName("total_tracks")
    @Expose
    val total_tracks: String = ""

    @SerializedName("type")
    @Expose
    val type: String = ""

    @SerializedName("uri")
    @Expose
    val uri: String = ""
}

class Artista {
    @SerializedName("external_urls")
    @Expose
    val external_urls: List<ExternalUrls> = ArrayList()

    @SerializedName("href")
    @Expose
    val href: String = ""

    @SerializedName("name")
    @Expose
    val name: String = ""

    @SerializedName("type")
    @Expose
    val type: String = ""

    @SerializedName("uri")
    @Expose
    val uri: String = ""
}

class ExternalUrls {
    @SerializedName("spotify")
    @Expose
    val spotify: String = ""
}

class Images {
    val height: String = ""
    val url: String = ""
    val width: String = ""
}
