package prueba.omnidoc.alan.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TrackModel {
    var href: String = ""
    var items: List<Track> = ArrayList()
    var limit: String = ""
    var next: String = ""
    var offset: String = ""
    var previous: String = ""
    var total: String = ""
}

class Track : Serializable {
    @SerializedName("artists")
    @Expose
    val artists: List<Artist> = ArrayList()

    @SerializedName("external_urls")
    @Expose
    val external_urls: ExternalUrlsTrack = ExternalUrlsTrack()

    @SerializedName("href")
    @Expose
    val href: String = ""

    @SerializedName("id")
    @Expose
    val id: String = ""

    @SerializedName("name")
    @Expose
    val name: String = ""

    @SerializedName("type")
    @Expose
    val type: String = ""

    @SerializedName("uri")
    @Expose
    val uri: String = ""

    @SerializedName("available_markets")
    @Expose
    val available_markets: List<String> = ArrayList()

    @SerializedName("disc_number")
    @Expose
    val disc_number: String = ""

    @SerializedName("duration_ms")
    @Expose
    val duration_ms: String = ""

    @SerializedName("explicit")
    @Expose
    val explicit: String = ""

    @SerializedName("is_local")
    @Expose
    val is_local: String = ""

    @SerializedName("preview_url")
    @Expose
    val preview_url: String = ""

    @SerializedName("track_number")
    @Expose
    val track_number: String = ""
}

class Artist {
    @SerializedName("external_urls")
    @Expose
    val external_urls: List<ExternalUrls> = ArrayList()

    @SerializedName("href")
    @Expose
    val href: String = ""

    @SerializedName("id")
    @Expose
    val id: String = ""

    @SerializedName("name")
    @Expose
    val name: String = ""

    @SerializedName("type")
    @Expose
    val type: String = ""

    @SerializedName("uri")
    @Expose
    val uri: String = ""
}

class ExternalUrlsTrack {
    @SerializedName("spotify")
    @Expose
    val spotify: String = ""
}