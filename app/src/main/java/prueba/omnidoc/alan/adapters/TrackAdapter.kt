package prueba.omnidoc.alan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.models.Track

abstract class TrackAdapter(val context: Context, var arrayList: ArrayList<Track>) :
    RecyclerView.Adapter<TrackAdapter.RecyclerViewHolder>() {
    inner class RecyclerViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.view_holder_album_track, parent, false)
        ) {

        private var cardView: CardView? = null
        private var imgAlbumTrack: TextView? = null
        private var txtNameTrack: TextView? = null
        private var txtTimeTrack: TextView? = null

        init {
            cardView = itemView.findViewById(R.id.cardView)
            imgAlbumTrack = itemView.findViewById(R.id.imgAlbumTrack)
            txtNameTrack = itemView.findViewById(R.id.txtNameTrack)
            txtTimeTrack = itemView.findViewById(R.id.txtTimeTrack)
        }

        fun bind(track: Track) {
            txtNameTrack!!.text = track.name
            txtTimeTrack!!.text = track.duration_ms
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val holder = RecyclerViewHolder(inflater, parent)

        return holder
    }

    override fun getItemCount(): Int = arrayList.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val track: Track = arrayList[position]
        holder.bind(track)
    }

    abstract fun getList(arrayList: Track)
}