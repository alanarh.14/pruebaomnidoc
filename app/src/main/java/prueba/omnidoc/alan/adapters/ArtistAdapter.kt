package prueba.omnidoc.alan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.models.ArtistModel

abstract class ArtistAdapter(val context: Context, var arrayList: List<ArtistModel>) :
    RecyclerView.Adapter<ArtistAdapter.RecyclerViewHolder>() {
    inner class RecyclerViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.view_holder_artist, parent, false)
        ) {

        private var cardView: CardView? = null
        private var pgWaiting: ProgressBar? = null
        private var imgArtist: ImageView? = null
        private var txtNameArtist: TextView? = null

        init {
            cardView = itemView.findViewById(R.id.cardView)
            pgWaiting = itemView.findViewById(R.id.pgWaiting)
            imgArtist = itemView.findViewById(R.id.imgArtist)
            txtNameArtist = itemView.findViewById(R.id.txtNameArtist)
        }

        fun bind(artist: ArtistModel) {
            txtNameArtist!!.text = artist.name

            if (artist.imgArtist.isNotEmpty()) {
                Picasso.get().load(artist.imgArtist).into(imgArtist)
                pgWaiting!!.visibility = View.GONE
                imgArtist!!.visibility = View.VISIBLE
            }

            cardView!!.setOnClickListener {
                getList(artist)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val holder = RecyclerViewHolder(inflater, parent)
        holder.setIsRecyclable(true)

        return holder
    }

    override fun getItemCount(): Int = arrayList.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val artist: ArtistModel = arrayList[position]
        holder.bind(artist)
    }

    abstract fun getList(arrayList: ArtistModel)

}
