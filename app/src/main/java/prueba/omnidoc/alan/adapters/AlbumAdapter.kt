package prueba.omnidoc.alan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.models.Album
import prueba.omnidoc.alan.models.ArtistModel

abstract class AlbumAdapter(val context: Context, var arrayList: ArrayList<Album>) :
    RecyclerView.Adapter<AlbumAdapter.RecyclerViewHolder>() {
    inner class RecyclerViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(R.layout.view_holder_album, parent, false)
        ) {
        private var cardView: CardView? = null
        private var imgAlbum: ImageView? = null
        private var txtNameAlbum: TextView? = null
        private var txtNameArtist: TextView? = null
        private var txtReleaseDate: TextView? = null
        private var txtTotalTracks: TextView? = null

        init {
            cardView = itemView.findViewById(R.id.cardView)
            imgAlbum = itemView.findViewById(R.id.imgAlbum)
            txtNameAlbum = itemView.findViewById(R.id.txtNameAlbum)
            txtNameArtist = itemView.findViewById(R.id.txtNameArtist)
            txtReleaseDate = itemView.findViewById(R.id.txtReleaseDate)
            txtTotalTracks = itemView.findViewById(R.id.txtTotalTracks)
        }

        fun bind(album: Album) {
            if (album.images.isNotEmpty()) {
                Picasso.get().load(album.images[1].url).into(imgAlbum)
            }

            txtNameAlbum!!.text = album.name
            txtReleaseDate!!.text = album.release_date
            txtTotalTracks!!.text = album.total_tracks
            txtNameArtist!!.text = album.artists[0].name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val holder = RecyclerViewHolder(inflater, parent)

        return holder
    }

    override fun getItemCount(): Int = arrayList.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val album: Album = arrayList[position]
        holder.bind(album)
    }

    abstract fun getList(arraList: Album)

    fun refreshData(arraList: ArrayList<Album>) {
        this.arrayList.clear()
        this.arrayList.addAll(arraList)

        notifyDataSetChanged()
    }
}