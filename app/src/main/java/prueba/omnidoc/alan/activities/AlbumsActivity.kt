package prueba.omnidoc.alan.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.adapters.AlbumAdapter
import prueba.omnidoc.alan.databinding.ActivityAlbumsBinding
import prueba.omnidoc.alan.interfaces.APIService
import prueba.omnidoc.alan.models.Album
import java.lang.Exception

class AlbumsActivity : AppCompatActivity() {
    lateinit var albumAdapter: AlbumAdapter

    private lateinit var binding: ActivityAlbumsBinding
    private var albumResponse: ArrayList<Album> = ArrayList()
    private var disposable: Disposable? = null
    private val apiService by lazy {
        APIService.create()
    }

    var idArtista = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlbumsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            idArtista = intent!!.extras!!.getString("idArtista")!!
        } catch (e: Exception) {
            idArtista = ""
        }

        parseJSON()

        with(binding) {
            setSupportActionBar(toolbar)

            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { finish() }

            var drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back, null)
            drawable = DrawableCompat.wrap(drawable!!)
            DrawableCompat.setTint(
                drawable,
                ContextCompat.getColor(this@AlbumsActivity, R.color.white)
            )
            supportActionBar!!.setHomeAsUpIndicator(drawable)
            supportActionBar!!.title = "Albums"

            albumAdapter = object : AlbumAdapter(this@AlbumsActivity, albumResponse) {
                override fun getList(album: Album) {
                    val intent = Intent(this@AlbumsActivity, TrackActivity::class.java)
                    intent.putExtra("idAlbum", album.id)
                    intent.putExtra("imageAlbum", album.images[1].url)
                    startActivity(intent)
                }
            }

            recyclerView.apply {
                layoutManager = LinearLayoutManager(this@AlbumsActivity)
                setHasFixedSize(true)
                adapter = albumAdapter
            }
        }
    }

    private fun parseJSON() {
        disposable = apiService.getData(idArtista)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                albumResponse.clear()
                albumResponse.addAll(result.items)
                albumAdapter.notifyDataSetChanged()
            },
                { error ->
                    Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                })
    }
}