package prueba.omnidoc.alan.activities

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.squareup.picasso.Picasso
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.databinding.ActivityTrackDetailBinding
import prueba.omnidoc.alan.models.Artist
import prueba.omnidoc.alan.models.Track
import java.lang.Exception

class TrackDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTrackDetailBinding

    var imageAlbum = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            imageAlbum = intent!!.extras!!.getString("imageAlbum")!!
        } catch (e: Exception) {
            imageAlbum = ""
        }

        val track: Track = intent!!.extras!!.getSerializable("track") as Track

        with(binding) {
            setSupportActionBar(toolbar)

            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { finish() }

            var drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back, null)
            drawable = DrawableCompat.wrap(drawable!!)
            DrawableCompat.setTint(
                drawable,
                ContextCompat.getColor(this@TrackDetailActivity, R.color.white)
            )
            supportActionBar!!.setHomeAsUpIndicator(drawable)
            supportActionBar!!.title = "Detalle"

            if (imageAlbum.isNotEmpty()) {
                Picasso.get().load(imageAlbum).into(imgAlbum)
            }

            txtTrackNumber.text = "Numero de track: ${track.track_number}"
            txtNameTrack.text = "Nombre: ${track.name}"

            if (track.artists.isNotEmpty()) {
                txtTextArtist.text = "Artista(s)"
                txtTextArtist.visibility = View.VISIBLE

                val artistList: ArrayList<Artist> = ArrayList()
                artistList.addAll(artistList)

                if (lytArtist.visibility == View.GONE) {
                    lytArtistBtn.visibility = View.VISIBLE

                    val sizeInDP = 2
                    val marginInDp = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, sizeInDP.toFloat(), resources
                            .displayMetrics
                    ).toInt()
                    val paramsLyt = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        1f
                    )

                    paramsLyt.setMargins(marginInDp, marginInDp, marginInDp, marginInDp)
                    var valorIni = 0
                    var cuenta = 0
                    var lytHoriPro: LinearLayout? = null

                    for (x in artistList) {
                        valorIni++
                        cuenta++
                        val txtArtist = TextView(this@TrackDetailActivity)

                        if (valorIni == 1) {
                            lytHoriPro = LinearLayout(this@TrackDetailActivity)
                        }

                        lytHoriPro!!.orientation = LinearLayout.HORIZONTAL

                        txtArtist.text = x.name.trim()
                        txtArtist.setTextColor(Color.BLACK)
                        txtArtist.isAllCaps = false
                        txtArtist.textSize = 22f
                        txtArtist.layoutParams = paramsLyt
                        txtArtist.setTextAppearance(
                            this@TrackDetailActivity,
                            android.R.style.TextAppearance_Material
                        )

                        if (valorIni <= 2) {
                            lytHoriPro.addView(txtArtist)
                            if (cuenta == artistList.size) {
                                lytArtist.addView(lytHoriPro)
                            } else if (valorIni == 2) {
                                valorIni = 0
                                lytArtist.addView(lytHoriPro)
                            }
                        }
                    }
                } else lytArtist.visibility = View.GONE
            }
        }
    }
}