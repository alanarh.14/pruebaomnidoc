package prueba.omnidoc.alan.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import prueba.omnidoc.alan.adapters.ArtistAdapter
import prueba.omnidoc.alan.databinding.ActivityMainBinding
import prueba.omnidoc.alan.models.ArtistModel

class MainActivity : AppCompatActivity() {
    lateinit var artistAdapter: ArtistAdapter

    private lateinit var binding: ActivityMainBinding

    val artistList = listOf(
        ArtistModel(
            "0vR2qb8m9WHeZ5ByCbimq2",
            "https://i.scdn.co/image/ab6761610000e5eb39febc3fd4e15ba709c7cdac",
            "Reik"
        ),
        ArtistModel(
            "0L8ExT028jH3ddEcZwqJJ5?",
            "https://i.scdn.co/image/89bc3c14aa2b4f250033ffcf5f322b2a553d9331",
            "Red Hot Chilli Pepers"
        ),
        ArtistModel(
            "7dGJo4pcD2V6oG8kP0tJRR",
            "https://i.scdn.co/image/ab6761610000e5eb3860fe3d46ee8a4d70620007",
            "Eminem"
        ),
        ArtistModel(
            "6olE6TJLqED3rqDCT0FyPh",
            "https://i.scdn.co/image/a4e10b79a642e9891383448cbf37d7266a6883d6",
            "Nirvana"
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            setSupportActionBar(toolbar)
            supportActionBar!!.title = "Artistas"

            artistAdapter = object : ArtistAdapter(this@MainActivity, artistList) {
                override fun getList(arrayList: ArtistModel) {
                    val intent = Intent(this@MainActivity, AlbumsActivity::class.java)
                    intent.putExtra("idArtista", arrayList.id)
                    startActivity(intent)
                }
            }

            recyclerView.apply {
                layoutManager = GridLayoutManager(this@MainActivity, 2)
                setHasFixedSize(true)
                adapter = artistAdapter
            }
        }
    }
}