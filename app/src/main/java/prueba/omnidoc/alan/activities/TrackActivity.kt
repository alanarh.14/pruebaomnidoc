package prueba.omnidoc.alan.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import prueba.omnidoc.alan.R
import prueba.omnidoc.alan.adapters.TrackAdapter
import prueba.omnidoc.alan.databinding.ActivityTrackBinding
import prueba.omnidoc.alan.interfaces.APIServiceTracks
import prueba.omnidoc.alan.models.Track
import java.lang.Exception

class TrackActivity : AppCompatActivity() {
    lateinit var trackAdapter: TrackAdapter

    private lateinit var binding: ActivityTrackBinding
    private var trackResponse: ArrayList<Track> = ArrayList()
    private var disposable: Disposable? = null
    private val apiServiceTrack by lazy {
        APIServiceTracks.create()
    }

    var idAlbum = ""
    var imageAlbum = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            idAlbum = intent!!.extras!!.getString("idAlbum")!!
            imageAlbum = intent!!.extras!!.getString("imageAlbum")!!
        } catch (e: Exception) {
            idAlbum = ""
            imageAlbum = ""
        }

        parseJSON()

        with(binding) {
            setSupportActionBar(toolbar)

            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { finish() }

            var drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back, null)
            drawable = DrawableCompat.wrap(drawable!!)
            DrawableCompat.setTint(
                drawable,
                ContextCompat.getColor(this@TrackActivity, R.color.white)
            )
            supportActionBar!!.setHomeAsUpIndicator(drawable)
            supportActionBar!!.title = "Canciones"

            trackAdapter = object : TrackAdapter(this@TrackActivity, trackResponse) {
                override fun getList(track: Track) {
                    val intent = Intent(this@TrackActivity, TrackDetailActivity::class.java)
                    intent.putExtra("track", track)
                    intent.putExtra("imageAlbum", imageAlbum)
                    startActivity(intent)
                }
            }

            recyclerView.apply {
                layoutManager = LinearLayoutManager(this@TrackActivity)
                setHasFixedSize(true)
                adapter = trackAdapter
            }
        }
    }

    private fun parseJSON() {
        disposable = apiServiceTrack.getData(idAlbum)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                trackResponse.clear()
                trackResponse.addAll(result.items)
                trackAdapter.notifyDataSetChanged()
            },
                { error ->
                    Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                })
    }
}