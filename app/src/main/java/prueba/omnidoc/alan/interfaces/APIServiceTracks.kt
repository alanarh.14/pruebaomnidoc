package prueba.omnidoc.alan.interfaces

import io.reactivex.Observable
import prueba.omnidoc.alan.models.TrackModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface APIServiceTracks {
    @GET("v1/albums/{id}/tracks")
    fun getData(@Path("id") id: String): Observable<TrackModel>

    companion object {
        fun create(): APIServiceTracks {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.spotify.com")
                .build()

            return retrofit.create(APIServiceTracks::class.java)
        }
    }
}