package prueba.omnidoc.alan.interfaces

import io.reactivex.Observable
import prueba.omnidoc.alan.models.AlbumModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {
    @GET("v1/artists/{id}/albums?market=ES")
    fun getData(@Path("id") id: String): Observable<AlbumModel>

    companion object {
        fun create(): APIService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.spotify.com")
                .build()

            return retrofit.create(APIService::class.java)
        }
    }
}

